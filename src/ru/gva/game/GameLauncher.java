package ru.gva.game;

/**
 * Тестовый класс, который запускает игру.
 *
 * @author Gavrikov V. 15it18.
 */

public class GameLauncher {

    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
