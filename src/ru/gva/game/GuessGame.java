package ru.gva.game;


/**
 * Данный класс содержит методы игры.
 *
 * @author Gavrikov V. 15it18.
 */
public class GuessGame {
    Player p1;
    Player p2;
    Player p3;

    /**
     * Метод который запускает игру.
     */
    public void startGame() {

        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        int targetNumber = (int) (Math.random() * 10);

        System.out.println(" Я думаю это число между 0 и 9 ...");

        while (true) {
            System.out.println("Предполагаемое число " + targetNumber);
            
            playerResponses();
            
            deductionOfPlayersAssumptions();
            
            searchForTheWinner(targetNumber);
            
            if (p1.isRight || p2.isRight || p3.isRight) {
                System.out.println("У нас есть победитель ! ");
                System.out.println("Первый игрок прав ? " + p1.isRight);
                System.out.println("Второй игрок прав ? " + p2.isRight);
                System.out.println("Третий игрок прав ? " + p3.isRight);
                System.out.println("Игра закончена!");
                break;
            }
            System.out.println("Игрокам придется попробовать еще раз. ");
        }
    }

    /**
     * Метод ищет победителя(ей).
     * 
     * @param targetNumber загаданное число.
     */
    private void searchForTheWinner(int targetNumber) {
        if (p1.number_random == targetNumber) {
            p1.isRight = true;
        }

        if (p2.number_random == targetNumber) {
            p2.isRight = true;
        }

        if (p3.number_random == targetNumber) {
            p3.isRight = true;
        }
    }

    /**
     * Метод выводит предпологаемые ответы
     */
    private void deductionOfPlayersAssumptions() {
        System.out.println("Первый игрок предполагает: " + p1.number_random);
        System.out.println("Второй игрок предполагет: " + p2.number_random);
        System.out.println("Третий игрок предполагает: " + p3.number_random);
    }

    /**
     * Метод формирует предположения игроков.
     */
    private void playerResponses() {
        p1.guess();
        p2.guess();
        p3.guess();
    }
    
}
