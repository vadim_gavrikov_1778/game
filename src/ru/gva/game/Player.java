package ru.gva.game;

/**
 * Метод содержит объект Player
 *
 * @author Gavrikov V. 15it18
 */

public class Player {

    boolean isRight = false;
    int number_random;

    /**
     * Метод формирует ответ игрока
     */
    public void guess(){
        number_random = (int)(Math.random() * 10);
        print(number_random);
    }

    /**
     * Метод для вывода ответа игрока
     *
     * @param number_random
     */
    private void print (int number_random){
        System.out.println("Я догадываюсь " + number_random);
    }

}
